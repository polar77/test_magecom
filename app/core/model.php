<?php

class Model{

    public $userid=false;

    public function __construct(){
        $link = mysql_connect(HOSTNAME, USERNAME, PASSWORD) OR die('Error : ' . mysql_error());;
        mysql_select_db(DBNAME,$link);
    }

    private function query($query){
        return mysql_query($query);
    }

    private function fetch_array($query){
        return mysql_fetch_array($query);
    }

    public function getDataCategory($id_category=null){
        $while = (!empty($id_category)) ? (' WHERE `id_categ` = '. (int) $id_category) : '';
        $out=array();
        $result = $this->query('SELECT `ads`.`id` as `id`, `ads`.`text_ads` as `text_ads`, `categories`.`name_categ` as `name_categ` FROM `ads` LEFT JOIN  `categories` ON `ads`.`id_categ`=`categories`.`id`' . $while);

        while ($row = $this->fetch_array($result)) {
            $out[]=array('id'=>$row['id'], 'text'=>$row['text_ads'], 'name_categ'=>$row['name_categ']);
        }
        return $out;
    }

    public function getListCategory(){
        $out=array();
        $result = $this->query('SELECT * FROM `categories`');
        while ($row = $this->fetch_array($result)) {
            $out[]=array('id'=>$row['id'], 'name_categ'=>$row['name_categ']);
        }
        return $out;
    }

    public function getListMyAds($id_user){
        $out=array();
        $result = $this->query('SELECT `ads`.`id` as `id`, `ads`.`text_ads` as `text_ads`, `categories`.`name_categ` as `name_categ` FROM `ads` LEFT JOIN  `categories` ON `ads`.`id_categ`=`categories`.`id` WHERE `id_own` = ' . $id_user);
        while ($row = $this->fetch_array($result)) {
            $out[]=array('id'=>$row['id'], 'text'=>$row['text_ads'], 'name_categ'=>$row['name_categ']);
        }
        return $out;
    }

    public function AddAds($id_user, $id_categ, $text){
        //echo 'INSERT INTO `categories` (`id_own`, `id_categ`, `text_ads`) VALUES ('.$id_user.', '.$id_categ.', \''.$text.'\')';exit;
        $out = $this->query('INSERT INTO `ads` (`id_own`, `id_categ`, `text_ads`) VALUES ('.$id_user.', '.$id_categ.', \''.$text.'\')');
        return $out;
    }

    public function getListUsers()
    {
        $out=array();
        $result = $this->query('SELECT `id`, `name_user` FROM `users`');
        while ($row = $this->fetch_array($result)) {
            $out[]=array('id'=>$row['id'], 'name'=>$row['name_user']);
        }
        return $out;
    }

    public function AddCategory($add_category)
    {
        $out = $this->query('INSERT INTO `categories` (`name_categ`) VALUES (\''.$add_category.'\')');
        return $out;
    }

    public function getDataLogin($login)
    {
        $out=false;
        $result = $this->query('SELECT `id`, `pass_user` FROM users WHERE `name_user`=\''.$login.'\'');
        if ($row = $this->fetch_array($result)) {
            $out=$row['pass_user'];
            $this->userid=$row['id'];
        }
        return $out;
    }

    public function getDataLoginAdmin($login)
    {
        $out=false;
        $result = $this->query('SELECT `pass_admin` FROM admins WHERE `name_admin`=\''.$login.'\'');
        if ($row = $this->fetch_array($result)) {
            $out=$row['pass_admin'];
        }
        return $out;
    }
}
