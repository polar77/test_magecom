<?php
//session_save_path("c:\WebServers\\");
session_start();

define('ROOT', getcwd());
ini_set('display_errors', 1);
require_once ROOT . '/conf/db.php';

require_once ROOT . '/app/bootstrap.php';