-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Сен 18 2015 г., 09:47
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_olx`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_admin` varchar(60) NOT NULL,
  `pass_admin` varchar(60) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `admins`
--

INSERT INTO `admins` (`id`, `name_admin`, `pass_admin`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_own` int(11) NOT NULL,
  `id_categ` int(11) NOT NULL,
  `text_ads` text NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `id_categ` (`id_categ`),
  KEY `id_own` (`id_own`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `ads`
--

INSERT INTO `ads` (`id`, `id_own`, `id_categ`, `text_ads`) VALUES
(1, 1, 1, 'В продаже эксклюзивные миниатюрные чистокровные клубные щеночки йоркширского терьера с двух разных пометов. Есть девочки и мальчик. У них мордашки беби фейс. Есть светлее окрас и темнее. Щеночки привиты и вакцинированы, прошли обработки против всех паразитов. У низ полный пакет документов. Детки идеальны внешне м высоким качеством красоты по всем мировым стандартам и отличным крепким здоровьем.\r\nВсе вопросы по телефону.\r\nВышлем фото, покажем по скайпу. Доставим лично в любой город Украины.\r\nВсегда рады дать совет и консультации.\r\nВсе наши малыши очень красивые и яркие с хорошим, добрым и ласковым характером. '),
(2, 1, 4, 'Продажа 3х. комнатная квартира ЖК Патриотика "Подолье"\r\nОбщая площадь 81,3/45/10 до метро Осокорки 10 минут пешком, развитая инфраструктура, ТЦ Пирамида Аладин, детские сады, школа, квартиры сдаются полностью с ремонтом( сантехника, счетчки, радиаторы, м/п окна, межкомнатные двери).\r\nСдача дома декабрь 2015 года. Цена 65 400 у.е '),
(3, 2, 1, 'Изготавливаем добротные и качественные будки для собак. Утепленные и стандартные. Работаем с натуральными материалами (вагонка, блок-хаус,имитация бруса).\r\nА также изготовление металлических вольеров для собак. Предлагаем различные варианты (вагонка, имитация бруса, прут + кормушки, будки и т.д.) Все модели сварные, разборные.');

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_categ` varchar(60) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name_categ`) VALUES
(1, 'Собаки'),
(4, 'Квартиры');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_user` varchar(60) NOT NULL,
  `pass_user` varchar(60) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name_user`, `pass_user`) VALUES
(1, 'user', 'user'),
(2, 'user1', 'user1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
