<?php

class Controller_Admin extends Controller
{
    public function __construct()
    {
        $this->model = new Model();
        $this->view = new ViewTempl();
    }

    public function action()
    {
        if(!empty($_POST['logining']))
        {
            $_SESSION['loginadmin'] = trim($_POST['login']);
            $_SESSION['passwadmin'] = $_POST['passw'];
        }

        if(!empty($_SESSION['loginadmin'])) {
            $loginadmin=trim($_SESSION['loginadmin']);
            if (!($passwadmin = $this->model->getDataLoginAdmin($loginadmin, 'admins'))) {
                unset($_SESSION['loginadmin']);
                unset($_SESSION['passwadmin']);
                header('Location:index.php?contr=admin');
                exit;
            } else $flag_admin=true;
        }

        if(empty($_SESSION['loginadmin']) || ($loginadmin != $_SESSION['loginadmin']) || ($passwadmin != $_SESSION['passwadmin']))
        {
            $data['loginadmin_admin']='no';
            $this->view->genTempl('login', 'user_login', $data);
            exit();
        }


        if (!flag_admin) {
            header('Location:index.php');
            exit;
        }

        $act = isset($_GET['act']) ? $_GET['act'] : null;
        $templ_act = ($act == 'add')  ? 'add_categ' : 'list_users';

        if ($templ_act=='list_users'){

            $data = $this->model->getListUsers();

        }else{
            if (isset($_POST['add_category']) && trim($_POST['add_category'])){
                $this->model->AddCategory(trim($_POST['add_category']));
                header('Location:index.php?contr=admin');
                exit;
            }
        }
        $this->view->genTempl('admin', $templ_act, $data);
    }
}