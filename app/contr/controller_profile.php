<?php

class Controller_Profile extends Controller
{
    public function __construct()
    {
        $this->model = new Model();
        $this->view = new ViewTempl();
    }

    public function action()
    {
        if(!empty($_POST['logining']))
        {
            $_SESSION['login'] = trim($_POST['login']);
            $_SESSION['passw'] = $_POST['passw'];
        }

        if(!empty($_SESSION['login'])) {
            $login=trim($_SESSION['login']);
            if (!($passw = $this->model->getDataLogin($login))) {
                unset($_SESSION['login']);
                unset($_SESSION['passw']);
                header('Location:index.php?contr=profile');
                exit;
            }
        }

        if(empty($_SESSION['login']) || ($login != $_SESSION['login']) || ($passw != $_SESSION['passw']))
        {
            $data['login_admin']='no';
            $this->view->genTempl('login', 'user_login', $data);
            exit();
        }

        $act = isset($_GET['act']) ? $_GET['act'] : null;

        $templ_act = ($act == 'add')  ? 'list_add_ads' : 'list_my_ads';

        $id_user = $this->model->userid;

        if ($templ_act=='list_my_ads'){

            $data = $this->model->getListMyAds($id_user);

        }else{
            if (isset($_POST['text']) && trim($_POST['text'])){
                $this->model->AddAds($id_user, $_POST['id_category'], trim($_POST['text']));
                header('Location:index.php?contr=profile');
                exit;
            }
            $data['categ'] = $this->model->getListCategory();
        }
        $this->view->genTempl('profile', $templ_act, $data);
    }
}