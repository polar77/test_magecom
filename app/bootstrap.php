<?php
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';

class Bootstrap
{

    static public function go()
    {

        $getcontroller = isset($_GET['contr'])?$_GET['contr']:'catalog';

        switch ($getcontroller){
            case 'admin':
                include ROOT . '/app/contr/controller_admin.php';
                $controller = new Controller_Admin;
                break;
            case 'profile':
                include ROOT . '/app/contr/controller_profile.php';
                $controller = new Controller_Profile;
                break;
            default:
                include ROOT . '/app/contr/controller_catalog.php';
                $controller = new Controller_Catalog;
        }
        $controller->action();
    }

}

Bootstrap::go();
