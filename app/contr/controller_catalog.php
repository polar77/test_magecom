<?php

class Controller_Catalog extends Controller
{
    public function __construct()
    {
        $this->model = new Model();
        $this->view = new ViewTempl();
    }

    public function action()
    {
        $id_category = (isset($_POST['id_category'])&&$_POST['id_category']>0)?(int)$_POST['id_category']:null;

        $data['ads'] = $this->model->getDataCategory($id_category);
        $data['categ'] = $this->model->getListCategory();
        $data['activ_categ'] = ($id_category)?$id_category:false;

        $this->view->genTempl('catalog','catalog',$data);
    }
}